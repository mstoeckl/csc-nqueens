#!/usr/bin/env python3

from os import system

base = "common.ss" # everything uses this
options = {
  "block-random-backtrack":("fail.ss","nq-bt"),
  "fast-backtrack":("fast.ss","nq-bt"),
  "unlimited-backtrack":("fail.ss","nq-bt-ulimit"),
  "insideout-backtrack":("fail.ss","nq-bt-evert"),
  "outsidein-backtrack":("fail.ss","nq-bt-ext"),
  
  "funky-minconflicts":("funky.ss","nq-mc"),
  "fast-minconflicts":("fast.ss","nq-mc"),
  "perm-minconflicts":("fast.ss","nq-mc-perm")
  
  }

template = """

(define load (lambda _ #f))
(include "common.ss")
(include "{file}")
({operation} (vector-ref #(#f #t 'nope) (string->number (car (command-line-arguments)))) (string->number (cadr (command-line-arguments))))

"""

comp = "csc -vvv -f -optimize-level 5 {inx} -o {out}"

inp = "#t 100"

for k,v in options.items():
    vf,vk = v
    name = "timing/"+k+".ss"
    with open(name,"w") as f:
        f.write(template.format(file=vf,operation=vk))
    system(comp.format(inx=name,out="timing/"+k))
  
  

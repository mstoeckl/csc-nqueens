#!/usr/bin/env python3

"""
wherein I analyze all the logs

"""

import os
from statistics import stdev, mean
from collections import defaultdict
from math import sqrt

n32 = lambda x: x * int(round(sqrt(x)))
f100 = lambda x: 100

limitfns = {
    "block-random-backtrack.log":n32,
    "fast-backtrack.log":n32,
    "insideout-backtrack.log":n32,
    "outsidein-backtrack.log":n32,
    "unlimited-backtrack.log":lambda x: float("inf"),
    "perm-minconflicts.log":n32,
    "fast-minconflicts.log":f100,
    "funky-minconflicts.log":f100
}

codes = {
    "block-random-backtrack.log":"B",
    "fast-backtrack.log":"F",
    "insideout-backtrack.log":"I",
    "outsidein-backtrack.log":"O",
    "unlimited-backtrack.log":"U",
    "perm-minconflicts.log":"P",
    "fast-minconflicts.log":"M",
    "funky-minconflicts.log":"D"
}

header = r"""
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{crrrrrr}
Code & $N$ & Time & Limit & Steps & Restarts & Timeouts\tabularnewline
\hline
"""
line = r"{code} & ${n}$ & ${t:.3f}\pm{et:.3f}$ & ${l}$ & ${s:.1f}\pm{es:.1f}$ & ${r:.2f}\pm{er:.2f}$ & $\unit[{per}]{{\%}}$\tabularnewline"

middle = r"""
\end{tabular}~%
\begin{tabular}{crrrrrr}
Code & $N$ & Time & Limit & Steps & Restarts & Timeouts\tabularnewline
\hline 
"""

end = r"""
\end{tabular}%
}
"""

stats = open("stats.dat","w")
latex = open("stats.tex","w")
print(header,file=latex)
j = 0
for name in sorted(os.listdir()):
    if not name.endswith(".log"):
        continue
    with open(name) as f:
        data = [x.split() for x in f.readlines()]
    times = defaultdict(list)
    retries = defaultdict(list)
    steps = defaultdict(list)
    wins = defaultdict(int)
    fails = defaultdict(int)
    opts = set()
    for n,step,time in data:
        n = int(n)
        opts.add(n)
        limit = limitfns[name](n)
        step = int(step)
        time = float(time)
        if step < 0 or time < 0:
            # how to treat failures?
            fails[n] += 1
        else:
            wins[n] += 1
            times[n].append(time)
            retries[n].append(step // limit)            
            steps[n].append(step % limit)

    print(file=stats)
    print(name,file=stats)
    for n in sorted(list(opts)):
        t = times[n]
        b = wins[n]
        s = steps[n]
        r = retries[n]
        fmt = "n {:>7} times {:>7.3f} +/- {:>7.3f} sec. | steps {:>11.3f} +/- {:>11.3f} | lim {:>6} | retries {:>10.3f} +/- {:>10.3f} | fails {:>5}%"
        if len(s) >= 1:
            mt,st,ms,ss,mr,sr = mean(t), stdev(t), mean(s), stdev(s), mean(r),stdev(r)
        else:
            mt,st,ms,ss,mr,sr = 0,0,0,0,0,0
        print(fmt.format(n,mt,st,ms,ss, limitfns[name](n), mr,sr, int(fails[n])),file=stats)
        
        sname = name[:-4].replace("-","")
        #print(sname + "zztimes" + str(n) + "= {" + ", ".join([str(x) for x in t]) + "};")
        #print(sname + "zzsteps" + str(n) + "= {" + ", ".join([str(x) for x in s]) + "};")
        #print(sname + "zzfails = " + str(fails[n]) + ";")
        #print(sname + "zzcap = " + str(limitfns[name](n)) + ";")

        print(line.format(n=n,t=mt,et=st,l=limitfns[name](n),s=ms,es=ss,r=mr,er=mr,per=int(fails[n]),code=codes[name]),file=latex)
        j += 1
        if j == 43:
            print(middle,file=latex)
print(end,file=latex)
        
#!/usr/bin/env python3
# ^ Python 3.4.2+

""" Timing script for N-queens """

# basically, for every file in the directory, run file 100 times for each argument level
# doubling the argument every time, and killing if runtime exceeds 2 minutes.

import os
import subprocess
import time
from multiprocessing import Pool
from subprocess import STDOUT, TimeoutExpired, check_output as qx

def run(cmd,seconds):
    t0 = time.time()
    try:
        x = qx(cmd.split(), stderr=STDOUT, timeout=seconds).decode()
        delta = time.time() - t0
        # the last number in the output is the number of steps
        v = [j for j in x.split("\n") if j][-1]
        steps = int(v)
    except TimeoutExpired:
        delta = -1
        steps = -1
    return steps,delta

def handle(name):
    size = 4
    cmd = "./{name} 2 {n}"
    f = open(name + ".log","w")
    failures = 0
    successes = 0
    while failures < 0.2 * nruns:
        failures = 0
        size *= 2
        for i in range(nruns):
            steps,t = run(cmd.format(name=name,n=size),timelimit)
            if t < 0:
                failures += 1
            print(size, steps, t, file=f)
            print(size,steps,t)
    f.close()

timelimit = 1 # sec
nruns = 100 # runs/config

tasks = [name for name in os.listdir() if not "." in name]
for _ in Pool(2).imap(handle, tasks):
  pass

        

        
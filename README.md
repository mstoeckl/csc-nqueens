
      ^^^^
      |NN|
      \--/
      <==>    R5RS Scheme
       ||     N>=4 Queens
       ||     First Soln.
     _/__\_
    /______\

# Why?

For the 2015 University of Rochester course, Computation and Formal Systems.

# Running the code

Like so, to test the function `nq-mc` in `file.ss`.

    time echo '(load "file.ss") (nq-mc #t 10000) #f' | plt-r5rs > results.dat

You can also modify `rig.ss` to include what you want to test and run it with

    ./test file.ss > results.dat

# Description of the problem space

The goal in this assignment is to implement two solvers for the N-queens problem
on a regular, NxN chessboard. The first is implemented by a function called
(nq-mc bool intsize) which solves the n-queens problem using a conflict minimi-
zation algorithm, and if the first argument is #t, prints out a 1/0 array de-
picting the solving chessboard, followed by a compact board representation and
and the number of iterations needed to find that solution. When #f is the first
argument, only the compact representation/number of steps get printed; any other
value means that just the solution state is returned. The second solver is
called (nq-bt bool boardsize) and behaves identically, save that it relies,
fundamentally, on a backtracking search routine. In both cases, variance
reduction methods are permissible [e.g., restarts, heuristic ordering.]

# Research aims

The research goal is to determine the runtime with/without various heuristics
and ordering schemes. By the principle of maximum confusion, the following are
to be timed (averaged over 100 runs on cycle1 at various N, with a maximum time
of 50 sec/run.)

*  backtracking: random placements, no step limit, left to right
*  backtracking: random placements, left to right
*  backtracking: block random placements (prefer even nonets), left to right
*  backtracking: inside-out ordering and random placements
*  backtracking: outside-in ordering and random placements
*  min-conflicts: greedy heuristic, row based search
*  min-conflicts: greedy heuristic, row based search, diagonal-centric data
*  min-conflicts: permutation heuristic, row based search

# Detailed description of implementation and approach

The guiding principle in the design of these algorithms was if you write as if
you were dealing with Fortran, the resulting code will still be fast. So I did;
the algorithms are all standard implementations, although in some places I had
to modify code to account for comparatively slow operations (like `display`)
and most list-handling procedures. Correspondingly, the board state is an
array; the counters are arrays, the scratch buffers are arrays. (The only use
of `cons` is where `*-values` would have worked.) Because R5RS is being used,
unchecked operations were not available; R7RS would have provided bytevectors,
an improvement over the cache-unfriendly 8 bytes used to denote values in the
range 0-3.

I tested two (practical) variants of the min-conflicts code. The first does
not record the conflict set, instead recalculating it every time. It uses a
simple counting array for each board diagonal type to keep track of the number
of conflicts in each. The second is designed to make conflict set updates
reasonably fast, and stores the first two colliding elements on each diagonal.
(For n>=1000, there are almost never any triple collisions when a greedy
algorithm initializes; I've yet to see a quadruple collision.) Both are
initialized by a greedy starting heuristic that minimizes conflicts, following
the advice of a paper I read (doi:10.1109/69.317698). Since the main routine
is necessarily less space/time efficient than their implementation, I increased
the number of placement attempts in my implementation (trying 4n placements,
rather than 3.08n. Although I had initially written the code to only move one
queen at a time, swapping pairs proved much more time efficient and cut the
number of steps (array scans) in half.) Running min-conflicts on a random
permutation (with, as several sources have indicated, an average of 0.528
collisions/queen) was also implemented, although that tends to be much slower.

For the backtracking code, I used the same counter-oriented design, to make
placements and backtracking as cheap as possible. It uses n^2 memory to record
the available (randomly shuffled) options at each column; this made the code
cleaner than explicit recursion would have, and made it easier to implement the
variant technique, which prefers to place queens on the central thirds of each
edge. (As you can guess, that makes it impractical for first-solution timing,
since that effectively restricts the search space to _only_ include solutions
within those few columns.) For variance reduction, I abused the rules and
detected when the backtracking search was in a local optimimum (i.e., where
there are lots of options, none of which pan out.), retrying the search after
about n^(3/2) placements. I also have an untimed version of the code.

# List of Files

* funky.ss  - Diagonal-oriented min-conflicts solver.
* fast.ss   - Backtracking solver; normal min-conflicts solver.
* fail.ss   - Backtracking, various types.
* rig.ss    - A more precise timing system for the functions.
* common.ss - Basic code that all n-queens solvers use. (print,rand,shuf)

